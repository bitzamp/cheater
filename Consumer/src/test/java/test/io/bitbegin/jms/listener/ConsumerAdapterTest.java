package test.io.bitbegin.jms.listener;

import static org.easymock.EasyMock.createMock;
import static org.junit.Assert.assertNotNull;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import io.bitbegin.jms.listener.ConsumerAdapter;
import io.bitbegin.jms.listener.ConsumerListener;


public class ConsumerAdapterTest {
    
	private ApplicationContext context;
	
	private ConsumerAdapter adapter;
	private TextMessage message;
	private String json = "{\r\n" + 
			"  \"name\": Adatper,\r\n" + 
			"  \"surname\": Michael,\r\n" + 
			"  \"title\": \"henderit\",\r\n" + 
			"  \"created\": \"2018-06-08 23:22:09\"\r\n" + 
			"}";
	@Before
	public void setUp() throws Exception {
		
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		adapter = (ConsumerAdapter)context.getBean("consumerAdapter");
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToDB() {
		
		adapter.sendToDB(json);
		assertNotNull(json);
	}

}
