package test.io.bitbegin.jms.listener;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertNull;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import io.bitbegin.jms.listener.ConsumerListener;
public class ComsumerListenerTest {
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{\r\n" + 
			"  \"name\": Chukwudi,\r\n" + 
			"  \"surname\": Michael,\r\n" + 
			"  \"title\": \" reprehenderit\",\r\n" + 
			"  \"created\": \"2018-06-08 23:22:09\"\r\n" + 
			"}";
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener)context.getBean("consumerListener");
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testOnMessage() throws JMSException {
		 expect(message.getText()).andReturn(json);
		 replay(message);
		listener.onMessage(message);
		verify(message);
	}

}
