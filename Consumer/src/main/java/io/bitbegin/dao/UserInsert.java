package io.bitbegin.dao;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class UserInsert {
	 
	  @Autowired
	  JdbcTemplate jdbcTemplate ;
	 
	  @Autowired
	  public void setgeJdbcTemplate(JdbcTemplate jdbcTemplate) {
	  this.jdbcTemplate = jdbcTemplate;
	 }
	 
	  private static final String insertSql = "INSERT INTO employee (name, surname, title) VALUES (?, ?, ?)";

	 public void saveRecord(String name, String surname, String title) {
		
		 // define query arguments
		 Object[] params = new Object[] { name, surname, title };
		 
		// define SQL types of the arguments , Types.TIMESTAMP
		 int[] types = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		 int row = jdbcTemplate.update(insertSql, params, types);
		 System.out.println(row + " row inserted.");

	 }
}
