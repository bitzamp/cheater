package io.bitbegin.jms.listener;



import java.text.DateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.bitbegin.dao.UserInsert;

@Component
public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	@Autowired
	UserInsert userInsert;
	
	public void sendToDB(String json) {
		logger.info("Sending to MYSQL");
		
		JsonObject result = new Gson().fromJson(json, JsonObject.class);
//		String date = result.get("created").getAsString();
//		 DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
//
//	        //to convert Date to String, use format method of SimpleDateFormat class.
		//Date.valueOf("2010-01-01")
//	        String strDate = dateFormat.format(date);
		logger.info("Convert Json To String");
		userInsert.saveRecord(result.get("name").getAsString(), result.get("surname").getAsString(), result.get("title").getAsString());
		logger.info("Sent to MYSQL");
	}

}
