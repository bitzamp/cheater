package io.bitbegin.connect;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
@Component
public class ConnectorAdapter {

	@Autowired
	 DataSource dataSource;
	    
	   
	    @Bean
	    public DataSource getDataSource() {
	          DriverManagerDataSource ds = new DriverManagerDataSource();
	          ds.setDriverClassName("com.mysql.cj.jdbc.Driver");	    	  	    	  
	          //ds.setUrl("jdbc:mysql://google/GTDLL?cloudSqlInstance=ngwuaya:us-central1:gt8world&socketFactory=com.google.cloud.sql.mysql.SocketFactory&user=GTDLL&password=I1hDt1BMM***8100PAYpalNE986wb***8100PAYpalI1hDt1BMM***8100PAYpalNE986wb***8100PAYpal&useSSL=false");
	          ds.setUrl("jdbc:mysql://localhost/GTDLL?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
	    

	          ds.setUsername("GTDLL");
	    	  ds.setPassword("I1hDt1BMM***8100PAYpalNE986wb***8100PAYpalI1hDt1BMM***8100PAYpalNE986wb***8100PAYpal");
   	          return ds;
	    }
	  
	    
	  @Bean
	  public JdbcTemplate geJdbcTemplate(){
		  return new  JdbcTemplate(dataSource);
	  }
	 
	 @Bean
	 public NamedParameterJdbcTemplate geNamedParameterJdbcTemplate(){
	  return new NamedParameterJdbcTemplate(dataSource);
	 }
	 
}
